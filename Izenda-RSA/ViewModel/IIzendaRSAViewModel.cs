﻿using System.ComponentModel;
using System.Windows.Input;

namespace Izenda_RSA.ViewModel
{
    public interface IIzendaRSAViewModel : INotifyPropertyChanged
    {
        #region Properties
        ViewMode ViewMode { get; set; }

        string PublicKey { get; set; }

        string PrivateKey { get; set; }

        string CustomKey { get; set; }

        bool IsFormattedPublicKey { get; set; }

        bool IsFormattedCustomKey { get; set; }

        ICommand GenerateKeysCommand { get; }

        ICommand SwitchModeCommand { get; }
        #endregion
    }
}
