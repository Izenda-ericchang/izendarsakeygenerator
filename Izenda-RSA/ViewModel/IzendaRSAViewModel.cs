﻿using Izenda_RSA.Model;
using Izenda_RSA.Utilities;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;

namespace Izenda_RSA.ViewModel
{
    internal class IzendaRSAViewModel : IIzendaRSAViewModel
    {
        #region Variables
        private readonly RSAKeyGenerator _rsaKeyGenerator;
        private ViewMode _viewMode = ViewMode.NEW;
        private string _publicKey = string.Empty;
        private string _privateKey = string.Empty;
        private string _customKey = string.Empty;
        private string _tempCustomKeyholder = string.Empty;
        private bool _isFormattedPublicKey = false;
        private bool _isFormattedCustomKey = false;
        private ICommand _generateKeysCommand;
        private ICommand _switchModeCommand;
        private RSAKeyPair _izendaPublicRSAPair = null;
        #endregion

        #region Properties
        public ViewMode ViewMode
        {
            get => _viewMode;
            set
            {
                if (_viewMode != value)
                {
                    _viewMode = value;
                    OnPropertyChanged(nameof(ViewMode));
                }
            }
        }

        public string PublicKey
        {
            get => _publicKey;
            set
            {
                if (_publicKey != value)
                {
                    _publicKey = value;
                    OnPropertyChanged(nameof(PublicKey));
                }
            }
        }

        public string PrivateKey
        {
            get => _privateKey;
            set
            {
                if (_privateKey != value)
                {
                    _privateKey = value;
                    OnPropertyChanged(nameof(PrivateKey));
                }
            }
        }

        public string CustomKey
        {
            get => _customKey;
            set
            {
                if (_customKey != value)
                {
                    _customKey = value;

                    if (string.IsNullOrEmpty(_customKey))
                    {
                        _tempCustomKeyholder = string.Empty;
                        IsFormattedCustomKey = false;
                    }

                    OnPropertyChanged(nameof(CustomKey));
                }
            }
        }

        public bool IsFormattedPublicKey
        {
            get => _isFormattedPublicKey;
            set
            {
                if (_isFormattedPublicKey != value)
                {
                    _isFormattedPublicKey = value;

                    if (!string.IsNullOrWhiteSpace(_publicKey))
                        ToggleXMLFormattedKey();

                    OnPropertyChanged(nameof(IsFormattedPublicKey));
                }
            }
        }

        public bool IsFormattedCustomKey
        {
            get => _isFormattedCustomKey;
            set
            {
                if (_isFormattedCustomKey != value)
                {
                    _isFormattedCustomKey = value;

                    if (!string.IsNullOrWhiteSpace(_customKey))
                        ToggleXMLFormattedKey();

                    OnPropertyChanged(nameof(IsFormattedCustomKey));
                }
            }
        }

        public ICommand GenerateKeysCommand
        {
            get
            {
                if (_generateKeysCommand == null)
                    _generateKeysCommand = new RelayCommand(c => true, c => OnGenerateKeysCommand());

                return _generateKeysCommand;
            }
        }

        public ICommand SwitchModeCommand
        {
            get
            {
                if (_switchModeCommand == null)
                    _switchModeCommand = new RelayCommand(c => true, c => OnSwitchModeCommand());

                return _switchModeCommand;
            }
        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region CTOR
        public IzendaRSAViewModel() => _rsaKeyGenerator = new RSAKeyGenerator();
        #endregion

        #region Methods
        private void OnGenerateKeysCommand() => GenerateKeyPairCommand();

        private void GenerateKeyPairCommand()
        {
            _izendaPublicRSAPair = _rsaKeyGenerator.CreateRSAKeyPair();

            PublicKey = IsFormattedPublicKey ? _izendaPublicRSAPair.XMLPublicKey : _izendaPublicRSAPair.PublicKey;
            PrivateKey = _izendaPublicRSAPair.PrivateKey;
        }

        private void ToggleXMLFormattedKey()
        {
            if (ViewMode == ViewMode.NEW)
                PublicKey = IsFormattedPublicKey ? _izendaPublicRSAPair.XMLPublicKey : _izendaPublicRSAPair.PublicKey;
            else
            {
                if (IsFormattedCustomKey && !string.IsNullOrEmpty(_customKey))
                {
                    var rsaCustomKeyGen = new XmlFormatter(_customKey);

                    if (!string.IsNullOrEmpty(rsaCustomKeyGen.UserInputKey.XMLFormatPublicKey))
                    {
                        CustomKey = rsaCustomKeyGen.UserInputKey.XMLFormatPublicKey;
                        _tempCustomKeyholder = rsaCustomKeyGen.UserInputKey.PublicKey;
                    }
                    else
                    {
                        MessageBox.Show("Your Public RSA Key is incorrect. Please check your key and try again", "Izenda: Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        IsFormattedCustomKey = false;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(_customKey))
                        CustomKey = _tempCustomKeyholder;
                }
            }
        }

        private void OnSwitchModeCommand() => ViewMode = ViewMode == ViewMode.CONVERT ? ViewMode.NEW : ViewMode.CONVERT;

        protected void OnPropertyChanged([CallerMemberName] string name = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        #endregion
    }
}
