﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Security;
using System.IO;
using System.Text;

namespace Izenda_RSA.Model
{
    using BCPemWriter = Org.BouncyCastle.OpenSsl.PemWriter;

    internal class RSAKeyGenerator
    {
        #region Methods
        public RSAKeyPair CreateRSAKeyPair() 
        {
            var asymmetricCipherKeyPair = GenerateKeys(1024);
            var publicKey = GetKey(asymmetricCipherKeyPair.Public);
            var privateKey = GetKey(asymmetricCipherKeyPair.Private);
            var xmlFormatPublicKey = RSAKeyHelper.GetXmlFromKey(publicKey);

            return new RSAKeyPair(publicKey, privateKey, xmlFormatPublicKey);
        }

        private AsymmetricCipherKeyPair GenerateKeys(int keySize)
        {
            var rsaKeyPairGenerator = new RsaKeyPairGenerator();
            rsaKeyPairGenerator.Init(new KeyGenerationParameters(new SecureRandom(), keySize));

            return rsaKeyPairGenerator.GenerateKeyPair();
        }

        private string GetKey(AsymmetricKeyParameter key)
        {
            var stringBuilder = new StringBuilder();
            var stringWriter = new StringWriter(stringBuilder);
            var pemWriter = new BCPemWriter(stringWriter);

            pemWriter.WriteObject(key);
            pemWriter.Writer.Flush();
            stringWriter.Close();

            return stringBuilder.ToString();
        }
        #endregion
    }
}
