﻿namespace Izenda_RSA.Model
{
    internal class RSAKeyPair
    {
        #region Properties
        public string PublicKey { get; }

        public string PrivateKey { get; }

        public string XMLPublicKey { get; }
        #endregion

        #region CTOR
        public RSAKeyPair(string publicKey, string privateKey, string xmlFormatPublicKey)
        {
            PublicKey = publicKey;
            PrivateKey = privateKey;
            XMLPublicKey = xmlFormatPublicKey;
        } 
        #endregion
    }
}
