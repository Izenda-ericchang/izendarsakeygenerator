﻿namespace Izenda_RSA.Model
{
    internal class UserInputPublicKey
    {
        #region Properties
        public string PublicKey { get; }

        public string XMLFormatPublicKey { get; }
        #endregion

        #region CTOR
        public UserInputPublicKey(string publicKey, string xmlFormatPublicKey)
        {
            PublicKey = publicKey;
            XMLFormatPublicKey = xmlFormatPublicKey;
        }
        #endregion
    }
}
