﻿namespace Izenda_RSA.Model
{
    internal class XmlFormatter
    {
        #region Properties
        public UserInputPublicKey UserInputKey { get; }
        #endregion

        #region CTOR
        public XmlFormatter(string customKey) => UserInputKey = new UserInputPublicKey(customKey, RSAKeyHelper.GetXmlFromKey(customKey));
        #endregion
    }
}
