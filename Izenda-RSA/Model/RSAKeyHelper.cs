﻿using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using System;
using System.IO;
using System.Security.Cryptography;

namespace Izenda_RSA.Model
{
    internal static class RSAKeyHelper
    {
        #region Methods
        public static string GetXmlFromKey(string inKey)
        {
            return GetXmlRsaKey(inKey, (object obj) => getRsa(obj), (RSA rsa) => rsa?.ToXmlString(false));
        }

        private static RSA getRsa(object obj) => obj is RsaKeyParameters rsaKeyParameters ? DotNetUtilities.ToRSA(rsaKeyParameters) : obj as RSA;

        private static string GetXmlRsaKey(string pem, Func<object, RSA> getRsa, Func<RSA, string> getKey)
        {
            string str = string.Empty;
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var streamReader = new StreamReader(memoryStream))
                    {
                        streamWriter.Write(pem);
                        streamWriter.Flush();
                        memoryStream.Position = 0;

                        try
                        {
                            using (RSA rSA = getRsa((new Org.BouncyCastle.OpenSsl.PemReader(streamReader)).ReadObject()))
                            {
                                str = getKey(rSA);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.ToString());
                        }
                    }
                }
            }
            return str;
        } 
        #endregion
    }
}
