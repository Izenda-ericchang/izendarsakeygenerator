﻿using System;
using System.Windows.Input;

namespace Izenda_RSA.Utilities
{
    internal class RelayCommand : ICommand
    {
        #region Variables
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;
        #endregion

        #region Events
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        } 
        #endregion

        #region CTOR
        public RelayCommand(Predicate<object> canExecute, Action<object> execute)
        {
            _canExecute = canExecute;
            _execute = execute;
        }
        #endregion

        #region Methods
        public bool CanExecute(object parameter) => _canExecute(parameter);

        public void Execute(object parameter) => _execute(parameter);
        #endregion
    }
}
