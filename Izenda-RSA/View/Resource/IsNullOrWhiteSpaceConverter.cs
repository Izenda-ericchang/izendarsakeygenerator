﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Izenda_RSA.View.Resource
{
    public class IsNullOrWhiteSpaceConverter : IValueConverter
    {
        #region Methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var input = (string)value;

            return !string.IsNullOrWhiteSpace(input);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotSupportedException(); 
        #endregion
    }
}
