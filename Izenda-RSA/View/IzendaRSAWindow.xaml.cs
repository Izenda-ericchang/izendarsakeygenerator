﻿using Izenda_RSA.ViewModel;
using System.Windows;

namespace Izenda_RSA
{
    public partial class IzendaRSAWindow : Window
    {
        #region CTOR
        public IzendaRSAWindow()
        {
            InitializeComponent();

            var dataContext = new IzendaRSAViewModel();
            DataContext = dataContext;
        } 
        #endregion
    }
}
